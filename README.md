# ESPlay Game

## About
ESPlay is a simple browser game designed to test the theory that some people can predict the future by reading their own future mind. In the game, you are tasked with selecting between 5 images/options, and after you select one, the "correct" option will be revealed, accompanied by a unique audiovisual cue. Prior to making your guess, you can play this same audiovisual cue to try to pinpoint that future moment in time when the answer will be revealed to you to see if you can read the answer from your future mind. 

## Usage
The esplay.html file contains the entire game, so you can simply open that file with your browser of choice to play it. Alternatively, it is also free to play at: https://sacrificialprawn.itch.io/esplay

The game can be played with your mouse/touchscreen or with the keyboard, with the below controls:
- Arrow keys: navigate options
- Enter: select option
- Spacebar: play audiovisual cue
- 1,2,3,4,5: select corresponding choice
- s: enable/disable all-time stats/stat saving to localstorage
- d: highlight option to delete saved stats from localstorage (Enter can then be pressed to actually delete them)

Note that within the game you can enable saving your game stats to your browser's localstorage area. If you clear your browser's localstorage (which you should probably do periodically), the stats will be lost, so if you are particularly keen to save your stats, consider recording them elsewhere rather than relying on the save function. Also, note that you can disable stats saving at any time and delete any stats currently saved to your browser's localstorage area. All saving is done locally; the game does not transfer any data away from your computer. 

## License
This game is released under the MIT license.
